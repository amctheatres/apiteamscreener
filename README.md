# AMC API Team Interview Screener #

*Background – Given a Bookstore API that tracks the following simplified data:*

**Books**

* Title
* Description
* Publish Date
* Cover Image URL
* Authors
* Categories
* Reviews

**Authors**

* First Name
* Last Name
* Headshot Image URL

**Categories**

* Name

**Reviews**

* Reviewer Name
* Review Text
* Rating
* Publish Date


### Questions ###
1. How would the Book object be represented in JSON?
2. Provide a few RESTful URLs to GET Book resources that a API consumer is likely to find helpful.
3. What URL would you use to fetch a list of the first 10 books by “Scott Adams” in the “Audio Book” category?

#### Notes ####
* 15-20 minutes should be enough time to spend on answers. 
* Your answers can be submitted in a text document.
* Please do not submit your answers in code form.